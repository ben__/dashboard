#!/bin/bash

pid1=0
pid2=0

(
  cd ../Client

  npm i

  npm run build --prod
) & pid1=$!

(
  cd ../Server

  npm i

  npm run build;
) & pid2=$!

wait $pid1; r1=$?;
wait $pid2; r2=$?;