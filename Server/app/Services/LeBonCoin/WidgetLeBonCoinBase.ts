import { Router } from 'express';
import { WidgetBase } from '../WidgetBase';
import { LeBonCoinServiceInstance } from './service';

export abstract class LeBonCoinWidget extends WidgetBase {
  protected _service = LeBonCoinServiceInstance;

  constructor(router: Router, path: string) {
    super(router, path);
    this._service.register(this);
  }
}