import { ApiInstance } from './Api/Api';
import { Request } from 'express'

const parseIP = (ip: string) => {
  let a = ip.split(':');
  let ipString = a[a.length - 1];

  if (ipString == "1") {
    return 'localhost';
  } else {
    return ipString;
  }
}

export class About {
  public static get(req: Request) {
    return {
      "client": {
        "host": req.headers['x-forwarded-for'] || parseIP(req.connection.remoteAddress)
      },
      "server": {
        "current_time": new Date().getTime(),
        "services": ApiInstance.about()
      }
    };
  }
}