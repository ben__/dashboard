import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private _listeners = {};

  constructor() {}

  get(name: string) {
    let l = this._listeners[name] as EventEmitter<any>;
    if (typeof l === "undefined") {
      l = this._listeners[name] = new EventEmitter<any>();
    }
    return l;
  }
}
