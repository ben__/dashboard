import { Component, OnInit } from '@angular/core';
import { WidgetCompBase } from '../WidgetBase';
import { ApiService } from 'src/app/Services/api.service';
import { EventService } from 'src/app/Services/event.service';
import { TimerService } from 'src/app/Services/timer.service';
import { GlobalPopupService } from 'src/app/global-popup/global-popup.service';
import { Observer } from 'rxjs';

@Component({
  selector: 'app-widget-user-epitech',
  templateUrl: './widget-user-epitech.component.html',
  styleUrls: ['./widget-user-epitech.component.scss']
})
export class WidgetUserEpitechComponent extends WidgetCompBase implements OnInit {
  data: any;

  constructor(api: ApiService,
              eventService: EventService,
              timer: TimerService,
              popup: GlobalPopupService) {
    super(eventService, timer, popup, api);
  }

  ngOnInit() {
    this.listen((observer: Observer<any>) => {
      this._api.intraEpitech.user().subscribe((data: any) => {
        observer.next(JSON.parse(data.data));
      });
    }, (result: any) => {
      this.loaded = true;
      this.data = result;
    });
  }

}
