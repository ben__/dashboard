import { IntraEpitechWidget } from './WidgetIntraEpitechBase';
import { Router } from 'express';
import { ResponseType } from '../../Protocol/Protocol';
import * as https from 'https';
import * as moment from 'moment';

class UserIntraEpitech extends IntraEpitechWidget {
  constructor(router: Router) {
    super(router, "/user");

    router.get('/get', (req, baseResp) => {
      let user = req["dbUserDef"];
      this._service.getUserInfos(user._id).then((doc) => {
        https.get(`https://intra.epitech.eu/auth-${doc.key}/user/?format=json`,
          (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
              data += chunk;
            });
            resp.on('end', () => {
                baseResp.json({
                type: ResponseType.ACCEPT,
                data: data
              });
            });
    
            resp.on('error', function(err) {
              baseResp.json({
                type: ResponseType.REJECT,
                message: "unable to connect to remote host"
              });
            });
          });
      }, () => {
        baseResp.json({
          type: ResponseType.REJECT,
          message: "Not connected to Epitech"
        });
      })
    });
  }

  about() {
    return {
      "name": "user_infos",
      "description": "affiche les infos d'un etudiant Epitech",
      "params": []
    }
  }

  widgetInfo() {
    return Promise.resolve({
      name: "User",
      iconUrl: "/assets/epitech/epitech.jpeg",
      description: "Get infos for a user given"
    });
  }
}

export let UserIntraEpitechInstance = new UserIntraEpitech(Router());