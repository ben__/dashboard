import {
  Component,
  ComponentFactoryResolver,
  Injector,
  ComponentRef, 
  AfterContentInit,
  NgZone} from '@angular/core';
import { PopupResponse, PopupResponseType } from './../global-popup/global-popup.component';
import { WidgetSelectorComponent, ResultData } from './../widget-selector/widget-selector.component';
import { GlobalPopupService } from '../global-popup/global-popup.service';
import { ApiService } from '../Services/api.service';
import { WidgetWeatherComponent } from './Widgets/widget-weather/widget-weather.component';
import * as _ from "lodash";
import { WidgetCompBase } from './Widgets/WidgetBase';
import { WidgetSelectorDirective } from './widget-selector.directive';
import { MatGridList } from '@angular/material';
import { ViewChild, ElementRef } from '@angular/core';
import { config } from 'rxjs';
import { EventService } from '../Services/event.service';
import { WidgetEpitechModuleComponent } from './Widgets/widget-epitech-module/widget-epitech-module.component';
import { WidgetLbcSearchComponent } from './Widgets/widget-lbc-search/widget-lbc-search.component';
import { SettingItem, SettingTypes } from '../widget-settings/widget-settings.component';
import { WidgetUserEpitechComponent } from './Widgets/widget-user-epitech/widget-user-epitech.component';
import { WidgetRssComponent } from './Widgets/widget-rss/widget-rss.component';
import { WidgetAtomComponent } from './Widgets/widget-atom/widget-atom.component';
import { WidgetYoutubeChannelComponent } from './Widgets/widget-youtube-channel/widget-youtube-channel.component';

class WidgetFactory {
  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _injector: Injector) {}

  private _widgetMap = {
    "Weather": {
      "City": WidgetWeatherComponent
    },
    "Intra Epitech": {
      "Module": WidgetEpitechModuleComponent,
      "User": WidgetUserEpitechComponent
    },
    "LeBonCoin": {
      "Search": WidgetLbcSearchComponent
    },
    "Divers": {
      "RSS": WidgetRssComponent,
      "Atom": WidgetAtomComponent
    },
    "YouTube": {
      "Channel": WidgetYoutubeChannelComponent
    }
  }
  
  create(serviceName, widgetName) {
    return this._componentFactoryResolver
        .resolveComponentFactory(this._widgetMap[serviceName][widgetName] as any)
        .create(this._injector);
  }
}

interface WidgetDef {
  serviceName?: string,
  name?: string,
  color?: string,
  parent?: HTMLElement,
  componentRef?: ComponentRef<WidgetCompBase>,
  drag?: {dragging: boolean, data: any},
  dummy?: boolean,
  settings?: {}
}

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss'],
  providers: [WidgetSelectorDirective]
})
export class DashboardMainComponent implements AfterContentInit {
  public widgets = new Array<WidgetDef>()
  public  grid = {
    cols: 1,
    rowH: "250px"
  }
  private _factory: WidgetFactory;
  
  constructor(
  _componentFactoryResolver: ComponentFactoryResolver,
  _injector: Injector,
  private _popupService: GlobalPopupService,
  private _api: ApiService,
  private _zone: NgZone,
  _eventManager: EventService) {
    this._factory = new WidgetFactory(_componentFactoryResolver, _injector);

    let that = this;
    (this.widgets as any).internPush = function() {
      return Array.prototype.push.apply(this, arguments);
    };
    (this.widgets as any).internFirstPush = function() {
      (arguments[0] as WidgetDef).drag = {
        dragging: false,
        data: null
      };
      return Array.prototype.push.apply(this, arguments);
    };
    (this.widgets as any).moveAt = function(i: number, j: number) {
      let copy = [];
      let target = this[i];
      for (let k = 0; k < this.length; ++k) {
        if (k !== i)
          copy.push(this[k]);
      }
      this.splice(0, this.length);
      let isOk = false;
      for (let k = 0; k < copy.length; ++k) {
        if (k === j) {
          this.internPush(target);
          isOk = true;
        }
        this.internPush(copy[k]);
      }
      if (!isOk) this.internPush(target);
    };
    (this.widgets as any).deleteAt = function(i, save = true) {
      this.splice(i, 1);
      if (save)
        that.updateConfig();
    };
    this.widgets.push = function() {
      _.defer(() => { that.updateConfig(); });
      (arguments[0] as WidgetDef).drag = {
        dragging: false,
        data: null
      };
      return Array.prototype.push.apply(this, arguments);
    }
    _api.onLoginSuccess.subscribe(() => {
      _api.getConfig().subscribe((data: Array<WidgetDef>) => {
        this.widgets.splice(0, this.widgets.length);
        _.each(data, (wid: WidgetDef) => {
          wid.componentRef = 
            this._factory.create(wid.serviceName, wid.name) as ComponentRef<WidgetCompBase>;
          wid.parent = null;
          wid.componentRef.instance.loadSettings(wid.settings);
          delete wid.settings;
          (this.widgets as any).internFirstPush(wid);
        });
      })
    });
    _eventManager.get("onSettingsChange").subscribe(() => {
      that.updateConfig();
    });
  }

  @ViewChild("gridList") elemGridList: MatGridList;

  ngAfterContentInit() {
    let w = (this.elemGridList as any)._element.nativeElement.clientWidth;
    this._resizeHandler(w);
  }

  private updateConfig() {
    let serializable = _.map(this.widgets, (wid: WidgetDef) => {
      
      wid.settings = wid.componentRef.instance.settings;
      
      wid = _.reduce(wid, (result, value, key) => {
        if (key !== "componentRef" && key !== "parent" && key != "drag")
          if (value instanceof SettingItem)
            result[key] = _.reduce(value as SettingItem, (_res, _val ,_key) => {
              if (_key !== "notSerializable")
              _res[_key] = _val;
            return _res;
            }, {});
          else
            result[key] = value;
        return result;
      }, {});
      return wid;
    });
    this._api.setConfig(serializable).subscribe(() => {});
  }

  showWidgetSelector() {
    let ref = this._popupService.fire(WidgetSelectorComponent);

    ref.registerOnClose((rep: PopupResponse<ResultData>) => {
      if (rep.type === PopupResponseType.ACCEPT) {
        let componentRef = this._factory.create(rep.data.service.name, rep.data.widget.name);

        setTimeout(() => {
          let popup = (componentRef.instance as WidgetCompBase).showSettings();
          popup.registerOnClose((popupRep: PopupResponse<any>) => {
            if (popupRep.type === PopupResponseType.ACCEPT) {
              let widget: WidgetDef = {
                serviceName: rep.data.service.name,
                name: rep.data.widget.name,
                color: rep.data.service.color,
                parent: null,
                componentRef: componentRef as ComponentRef<WidgetCompBase>
              }
              _.defer(() => {
                this.widgets.push(widget);
              });
            }
          })
        }, 200);
      }
    });
  }

  private _resizeHandler(w) {
    const colSize = 250;

    _.defer(() => {this.grid.cols = Math.max(1, Math.floor(Math.max(1, w) / colSize))});
  }

  onGridResized(e) {
    let w = e.target.innerWidth;

    this._resizeHandler(w);
  }

  logoutClick() {
    this._api.logout();
  }

  drag = {
    _isDraggable: -1,
    dragging: false,
    setCurrentDraggable: (i) => { this.drag._isDraggable = i; },
    unsetCurrentDraggable: () => { this.drag._isDraggable = -1; },
    start: (evt, i) => {
      if (i !== this.drag._isDraggable) {
        evt.cancelDrag$.emit();
        return;
      }
      this.drag.dragging = true;
      this.widgets[i].drag = {
        dragging: true,
        data: {
          i: i,
          item: this.widgets[i]
        }
      };
    },
    move: (evt, i) => {},
    end: (evt, i) => {
      this.drag.dragging = false;
      this.widgets[i].drag = {
        dragging: false,
        data: null
      };
      this.updateConfig();
    },
    drop: (evt) => {},
    enter: (evt, i) => {
      if (evt.dropData && evt.dropData.i !== i) {
        this._zone.runOutsideAngular(() => {
          this.widgets['moveAt'](evt.dropData.i, i);
          evt.dropData.i = i;
        });
      }
    },
    leave: (evt, i) => {}
  }

  openSettings(evt, wid) {
    evt.preventDefault();
    evt.stopPropagation();
    wid.componentRef.instance.showSettings(wid)
  }

  deleteWidget(evt, i) {
    evt.preventDefault();
    evt.stopPropagation();
    let wid = this.widgets[i];
    (wid.parent as HTMLElement)
      .parentElement.parentElement.parentElement
      .classList.add('widget-close');
    setTimeout(() => {
      (wid.componentRef.instance as WidgetCompBase).destroy();
      (this.widgets as any).deleteAt(i);
    }, 480);
  }

  showSpinner(instance): boolean {
    if (typeof instance === "undefined") return false;
    return !instance.loaded;
  }
}
