import * as ProtocolBase from './Protocol';

export class ModuleIntraEpitechResponse extends ProtocolBase.BasicResponse {
  data: any;
}