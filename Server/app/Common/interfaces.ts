/*
** EPITECH PROJECT, 2018
** dashboard
** File description:
** interfaces.ts
*/
import * as express from 'express';
import { DBs } from '../Api/dbLib';
import { BasicResponse, ResponseType } from '../Protocol/Protocol';

export interface Serialisable {
  about: () => {}
  widgetInfo: (userId: string) => Promise<{}>
}

export class RoutedObject {
  constructor(protected _router: express.Router,
              protected _path: string) {}

  router() { return this._router; }
  path() { return this._path; }

  getUser(req) { return new Promise((resolve, reject) => {
    if (typeof req.sessionID !== 'undefined') {
      DBs.sessions.findOne({sessionId: req.sessionID}, (err, doc: any) => {
        if (doc !== null) {
          DBs.users.findOne({_id: doc.user}, (err, userDoc) => {
            if (userDoc !== null) {
              resolve(userDoc);
            } else {
              reject();
            }
          });
        } else {
          reject();
        }
      });
    } else {
      reject();
    }
  })}

  loginRequired = function() {
    let reject = (res) => {
      res.status(403).json({
        type: ResponseType.REJECT,
        message: "Not logged in"
      } as BasicResponse);
    };

    return (req: express.Request, res: express.Response, next) => {
      this.getUser(req).then((doc) => {
        req["dbUserDef"] = doc;
        next();
      }, (data) => {
        reject(res);
      })
    }
  }
}