import {
  Component, AfterViewInit, ComponentFactoryResolver,
  ApplicationRef, Injector, ElementRef,
  EmbeddedViewRef, EventEmitter } from '@angular/core';
import { GlobalPopupService } from './global-popup.service';
import * as _ from "lodash";

export enum PopupResponseType { REJECT, ACCEPT, CLOSED };
export interface PopupResponse<T> {
  type: PopupResponseType,
  data: T
};


export class PopupProperty {
  private _close: EventEmitter<any> = new EventEmitter();
  protected _arguments: Array<any> = [];

  setArguments(args: Array<any>) { this._arguments = args }

  accept<T>(__data?: T) {
    this._close.emit({
      type: PopupResponseType.ACCEPT,
      data: __data
    });
  }

  reject() {
    this._close.emit({type: PopupResponseType.REJECT});
  }

  close() {
    this._close.emit({type: PopupResponseType.CLOSED});
  }

  registerOnClose<T>(closeHandler: (rep: PopupResponse<T> | void) => void) { this._close.subscribe(closeHandler); }
}

@Component({
  selector: 'app-global-popup',
  templateUrl: './global-popup.component.html',
  styleUrls: ['./global-popup.component.scss']
})
export class GlobalPopupComponent implements AfterViewInit {
  private _counter: number = 0;
  private _animTime: number = 300; //ms

  constructor(private _popupService: GlobalPopupService,
              private _componentFactoryResolver: ComponentFactoryResolver,
              private _appRef: ApplicationRef,
              private _injector: Injector,
              private _me: ElementRef) {
  let me: HTMLElement = this._me.nativeElement;

  me.style.transform = "scale(0)";
  me.style.opacity = "0";
  me.style.display = "none";
  me.style.transition = `transform ${this._animTime}ms, opacity ${this._animTime}ms`;
}

  show() {
    let me: HTMLElement = this._me.nativeElement;

    me.style.display = "";
    _.defer(() => {
      me.style.opacity = "1";
      me.style.transform = "scale(1)";
    });
  }

  hide() {
    let me: HTMLElement = this._me.nativeElement;

    me.style.transform = "scale(0)";
    me.style.opacity = "0";
    setTimeout(() => {
      if (!this._counter)
        me.style.display = "none";
    }, this._animTime + 100);
  }

  onAddPopup(component: any, ...args: any[]): PopupProperty {
    this._counter++;
    const componentRef = this._componentFactoryResolver
      .resolveComponentFactory(component)
      .create(this._injector);
    
    // 2. Attach component to the appRef so this it's inside the ng component tree
    this._appRef.attachView(componentRef.hostView);
    
    // 3. Get DOM element from component
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;
    
    // 4. Append DOM element to the body
    let me: HTMLElement = this._me.nativeElement;
    let newElem = me.firstChild.appendChild(domElem);

    let instance: PopupProperty = <any> componentRef.instance;
    instance.setArguments(args);
    instance.registerOnClose(() => {
      this._counter--;
      setTimeout(() => {
        newElem.remove();
      }, this._animTime);
    });

    // 5. Return DOM Element 
    return instance;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._popupService.register(this);
  }
}
 