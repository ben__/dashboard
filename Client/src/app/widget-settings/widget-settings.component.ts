import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, OnInit } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { PopupProperty } from '../global-popup/global-popup.component';
import { ApiService } from '../Services/api.service';

export enum SettingTypes {
  STRING,
  NUMBER,
  AUTO
}

export interface SettingItemOptions {
  displayName?: string;
  optionsUrl?: string;
}

export class SettingItem {
  constructor(private _type: SettingTypes,
              public options: SettingItemOptions = {},
              private _data?: any) {}

  set(value: any, type = this._type) {
    this._data = value;
    this._type = type;
  }
  getType() { return this._type; }
  valueOf() {
    return this._data;
  }
}

export class SettingNode {
  children: SettingNode[];
  name: string;
  value: SettingItem;
  level: number;
  expandable: boolean;
  type: string;
}

@Injectable()
export class SettingsDatabase {
  dataChange = new BehaviorSubject<SettingNode[]>([]);

  get data(): SettingNode[] { return this.dataChange.value; }

  constructor() {}

  initialize(TREE_DATA: any) {
    let dataObject = TREE_DATA;

    const data = this.buildFileTree(dataObject, 0);
    this.dataChange.next(data);
  }

  buildFileTree(obj: object, level: number): SettingNode[] {
    return Object.keys(obj).reduce<SettingNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new SettingNode();
      node.name = (value.options || {}).displayName || key;

      if (value != null) {
        if (!(value instanceof SettingItem)) {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          let type: string;
          switch (typeof value) {
            case 'number': type = "number"; break;
            case 'string': type = "text"; break;
            default: type = "text";
          }
          node.type = type;
          node.value = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }
}

@Component({
  selector: 'app-widget-settings',
  templateUrl: './widget-settings.component.html',
  styleUrls: ['./widget-settings.component.scss'],
  providers: [SettingsDatabase]
})
export class WidgetSettingsComponent extends PopupProperty implements OnInit {

  treeControl: FlatTreeControl<SettingNode>;
  treeFlattener: MatTreeFlattener<SettingNode, SettingNode>;
  dataSource: MatTreeFlatDataSource<SettingNode, SettingNode>;

  constructor(private _database: SettingsDatabase,
              private _api: ApiService) {
    super();
  }

  transformer = (node: SettingNode, level: number) => {
    node.expandable = !!node.children;
    node.level = level;
    return node;
  }

  ngOnInit() {
    console.log(this._arguments[0])
    this._database.initialize(this._arguments[0]);
    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
                                              this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<SettingNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    this._database.dataChange.subscribe(data => this.dataSource.data = data);
    this.selectedNode = this._database.data[0];
  }

  private _getLevel = (node: SettingNode) => node.level;

  private _isExpandable = (node: SettingNode) => node.expandable;

  private _getChildren = (node: SettingNode): Observable<SettingNode[]> => observableOf(node.children);

  hasChild = (_: number, _nodeData: SettingNode) => _nodeData.expandable;

  types = SettingTypes;
  selectedNode: SettingNode;
  onClick(itm: SettingNode) {
    this.selectedNode = itm;
  }
  save() {
    this.accept(this._arguments[0]);
  }
}
