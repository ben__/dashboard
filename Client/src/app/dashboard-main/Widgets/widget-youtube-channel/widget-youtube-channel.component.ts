import { Component, OnInit } from '@angular/core';
import { WidgetCompBase } from '../WidgetBase';
import { ApiService } from 'src/app/Services/api.service';
import { EventService } from 'src/app/Services/event.service';
import { TimerService } from 'src/app/Services/timer.service';
import { GlobalPopupService } from 'src/app/global-popup/global-popup.service';
import { Observer } from 'rxjs';
import { SettingItem, SettingTypes } from 'src/app/widget-settings/widget-settings.component';

@Component({
  selector: 'app-widget-youtube-channel',
  templateUrl: './widget-youtube-channel.component.html',
  styleUrls: ['./widget-youtube-channel.component.scss']
})
export class WidgetYoutubeChannelComponent extends WidgetCompBase implements OnInit {
  data: any;

  constructor(api: ApiService,
              eventService: EventService,
              timer: TimerService,
              popup: GlobalPopupService) {
    super(eventService, timer, popup, api);

    this.settings["url"] = new SettingItem(SettingTypes.STRING, {
      displayName: "RSS stream url"
    }, "");
    this.settings["intervalMs"]._data = 120000
  }

  ngOnInit() {
    this.listen((observer: Observer<any>) => {
      this._api.youtube.channel(this.settings.url._data).subscribe((data: any) => {
        observer.next(data.data);
      });
    }, (result: any) => {
      console.log(result);
      this.loaded = true;
      this.data = result;
    });
  }

}