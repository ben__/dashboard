require('dotenv').config();
import { init } from "./init";
import { About } from "./about"
import { ApiInstance } from './Api/Api';
import { DBs } from './Api/dbLib';
import * as express from 'express';
import * as expressip from 'express-ip';
import * as uuid from 'uuid/v4';
import * as session from 'express-session';
import * as path from 'path';
import { AddressInfo } from 'net';

/* IMPORTANT: Force initilisation of all Services */
init();

let allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
}

const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.jpeg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
];

class Server {
  public  app: express.Express;
  private _port = 8080;

  public static bootstrap(): Server {
    return new Server();
  }

  constructor() {
    // Create expressjs application
    this.app = express();

    let sess: session.SessionOptions = {
      genid: (req) => {
        let sID: string;
        if (typeof req.sessionID !== 'undefined') {
          DBs.sessions.findOne({sessionId: req.sessionID}, (err, doc: any) => {
            if (doc !== null) {
              DBs.users.findOne({_id: doc.user}, (err, userDoc) => {
                if (userDoc !== null)
                  sID = req.sessionID;
                else
                  sID = uuid();
              });
            } else {
              sID = uuid();
            }
          });
        } else {
          sID = uuid();
        }
        while(sID === undefined) {
          require('deasync').runLoopOnce();
        }
        return sID;
      },
      secret: '1337',
      resave: false,
      saveUninitialized: true,
      cookie: {} as any
    }
    
    if (this.app.get('env') === 'production') {
      this.app.set('trust proxy', 1) // trust first proxy
      sess.cookie.secure = true // serve secure cookies
    }

    this.app.use(session(sess))
    this.app.use(allowCrossDomain);
    this.app.use(expressip().getIpInfoMiddleware);
    this.app.use(ApiInstance.path(), ApiInstance.router());
    this.app.get('/about.json', (req, res) => {
      res.send(JSON.stringify(About.get(req), undefined, 2));
    });
    this.app.get('*', (req, res) => {
      if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
        res.sendFile(path.resolve(`../Client/dist/dashboard/${req.url}`));
      } else {
        res.sendFile(path.resolve('../Client/dist/dashboard/index.html'));
      }
    });


    // Start the server on the provided port
    this.app.listen(this._port, () => console.log(`http is started ${this._port}`));
  }
}

//Bootstrap the server, so it is actualy started
const server = Server.bootstrap();
export default server.app;