FROM mongo

RUN apt update && apt install -yy curl make python gcc g++
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
      apt install -y nodejs

COPY . /dist

WORKDIR /dist/Client

RUN npm i

RUN npm run build --prod

WORKDIR /dist/Server

RUN npm i

RUN npm run build;

RUN echo "MONGO_INTERNAL=true" > .env

ENTRYPOINT [ "sh", "-c", "mongod --fork --logpath /var/log/mongodb/mongod.log; sleep 5; npm run start-prod" ]

HEALTHCHECK --interval=5m --timeout=10s \
  CMD curl -f http://localhost:5597/api/widgetssheet || exit 1