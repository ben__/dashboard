# Dashboard

## Client 

### Dev

```
cd Client/
./run_dev.sh
```

### Prod
```
cd Client/
./build_prod.sh
```

## Server

## Dev

```
cd Server/
./run_dev.sh
```

### Prod
```
cd Server/
./up_prod.sh ## Client must be built before !
```

### Standalone

```
docker pull retoxe/dashboard:1.0
```