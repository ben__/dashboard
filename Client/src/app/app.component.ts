import { Component, AfterViewInit } from '@angular/core';
import { GlobalPopupService } from './global-popup/global-popup.service';
import { LoginViewComponent } from './login-view/login-view.component';
import { ApiService } from './Services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'dashboard';
  constructor (private _popupService: GlobalPopupService, private _api: ApiService) {}

  ngAfterViewInit() {
    this._popupService.ready(() => {
      this._api.isLogged().then(null, () => {
        this._popupService.fire(LoginViewComponent);
      })
      this._api.needLogin.subscribe(() => {
        this._popupService.fire(LoginViewComponent);
      });
    });
  }
}
