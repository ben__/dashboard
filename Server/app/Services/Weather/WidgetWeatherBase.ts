import { Router } from 'express';
import { WidgetBase } from '../WidgetBase';
import { WeatherServiceInstance } from './service';

export abstract class WeatherWidget extends WidgetBase {
  protected _service = WeatherServiceInstance;

  constructor(router: Router, path: string) {
    super(router, path);
    this._service.register(this);
  }
}