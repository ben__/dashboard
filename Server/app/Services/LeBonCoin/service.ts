import { Router } from 'express';
import { ServiceBase } from '../ServiceBase';
import * as fs from 'fs';
import * as _ from 'lodash';
import { ResponseType } from '../../Protocol/Protocol';

let __catReducer = (res, value, key) => {
  if (key === "label")
    res.push(value);
  else if (typeof value === "object")
    res = _.reduce(value, __catReducer, res);
  return res;
};

let __regReducer = (res, value, key) => {
  if (key === "name")
    res.push(value);
  else if (typeof value === "object")
    res = _.reduce(value, __regReducer, res);
  return res;
};

const _categories = JSON.parse(fs.readFileSync('./node_modules/leboncoin-api/const/categories.json', 'utf8'))
const categories = _.reduce(_categories, __catReducer, []);
const _regions = JSON.parse(fs.readFileSync('./node_modules/leboncoin-api/const/regions.json', 'utf8'))
const regions = _.reduce(_regions, __regReducer, []);

class LeBonCoinService extends ServiceBase {
  constructor(router: Router) {
    super("LeBonCoin", router, "/leboncoin");
    
    this._color = "#ea6a2b";
    this._router.use(this.loginRequired());
    router.get('/', (req, resp) => {
      resp.json({
        message: "Welcome to the LeBonCoin service my friend :)"
      });
    });

    router.get('/regions', (req, resp) => {
      resp.json({
        type: ResponseType.ACCEPT,
        data: regions
      });
    });

    router.get('/categories', (req, resp) => {
      resp.json({
        type: ResponseType.ACCEPT,
        data: categories
      });
    });
  }

  isRegistered() { return Promise.resolve(true); }

  getApiInfo() {
    return {
      getRegion: (name: string) => {
        let res = _.find(_regions, (value) => {
          return (value.name === name)
        });
        return res.channel;
      },
      getCategory: (name: string) => {
        let getCat = (array: Array<any>): any => {
          for (let i = 0; i < array.length; ++i) {
            if (array[i].label === name) {
              return array[i];
            }
            if (array[i].subcategories) {
              let pret = getCat(array[i].subcategories);
              if (pret)
                return pret;
            }
          }
          return undefined;
        };

        let res = getCat(_categories);
        return res.channel;
      }
    }
  }
}

export let LeBonCoinServiceInstance = new LeBonCoinService(Router());
