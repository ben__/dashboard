export enum ResponseType {
  REJECT,
  ACCEPT
}

export class BasicResponse {
  public type: ResponseType;
  public data?: any;
  public message?: string;
}