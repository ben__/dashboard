import { TestBed, inject } from '@angular/core/testing';

import { GlobalPopupService } from './global-popup.service';

describe('GlobalPopupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalPopupService]
    });
  });

  it('should be created', inject([GlobalPopupService], (service: GlobalPopupService) => {
    expect(service).toBeTruthy();
  }));
});
