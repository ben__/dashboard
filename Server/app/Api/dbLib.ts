import * as mongoose from 'mongoose';

let db_infos = {
  host: 'dashboard-database-dev',
  username: 'admin',
  password: 'password',
  database: process.env.MONGO_DATABASE,
  port: process.env.MONGO_PORT
}

if (process.argv.length > 2 && process.argv[2] === "prod") {
  Object.assign(db_infos, {
    host: process.env.MONGO_HOST,
    username: process.env.MONGO_USERNAME,
    password: process.env.MONGO_PASSWORD
  })
}

const SessionModel = mongoose.model('Session', new mongoose.Schema({
  sessionId: String,
  user: require('mongodb').ObjectID //User unique ID in database
}));

const UserModel = mongoose.model('User', new mongoose.Schema({
  username: {
    type: String,
    unique: true // `email` must be unique
  },
  password: String,
  userconfig: String
}));

class DBLibary {
  public db = mongoose.connection;

  public users = UserModel;
  public sessions = SessionModel;

  constructor() {
    this.db.on('error', console.error.bind(console, 'connection error:'));
    this.db.once('open', function() {
      console.log('DB connected.')
    });
    if (process.env.MONGO_INTERNAL !== 'true') {
      mongoose.connect(`mongodb://${db_infos.username}:${db_infos.password}@${db_infos.host}:${db_infos.port}/${db_infos.database}?authSource=admin`, { useNewUrlParser: true });
    } else {
      mongoose.connect(`mongodb://localhost/dashboard`, { useNewUrlParser: true });
    }
  }

  add(name: string, options?: any) {
    if (typeof this[name] === "undefined") {
      const newModel = mongoose.model('name', new mongoose.Schema()); 
      this[name] = newModel;
      // if (options)
        //this[name].ensureIndex({ fieldName: 'sessionId', unique: true });
    }
    return this[name];
  }
}

export let DBs = new DBLibary();