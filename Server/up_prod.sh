if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

docker-compose -f docker/docker-compose-prod.yml down 
docker-compose -f docker/docker-compose-prod.yml build
docker-compose -f docker/docker-compose-prod.yml up -d --force-recreate api-prod