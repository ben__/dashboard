import { Router } from 'express';
import { ServiceBase } from '../ServiceBase';
import * as fs from 'fs';
import * as _ from 'lodash';
import { ResponseType } from '../../Protocol/Protocol';

class YouTubeService extends ServiceBase {
  constructor(router: Router) {
    super("YouTube", router, "/youtube");
    
    this._color = "#fd1100";
    this._router.use(this.loginRequired());
    router.get('/', (req, resp) => {
      resp.json({
        message: "Welcome to the YouTube service my friend :)"
      });
    });
  }

  isRegistered() { return Promise.resolve(true); }

  getApiInfo() {
    return {}
  }
}

export let YouTubeServiceInstance = new YouTubeService(Router());
