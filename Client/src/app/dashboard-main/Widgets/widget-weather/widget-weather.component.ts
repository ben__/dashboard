import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../Services/api.service';
import { WidgetCompBase } from '../WidgetBase';
import { TimerService } from 'src/app/Services/timer.service';
import { WeatherCityResponse } from '../../../../Protocol/Weather';
import { Observer } from 'rxjs';
import { GlobalPopupService } from '../../../global-popup/global-popup.service';
import { SettingItem, SettingTypes } from 'src/app/widget-settings/widget-settings.component';
import { EventService } from 'src/app/Services/event.service';

@Component({
  selector: 'app-widget-weather',
  templateUrl: './widget-weather.component.html',
  styleUrls: ['./widget-weather.component.scss']
})
export class WidgetWeatherComponent extends WidgetCompBase implements OnInit {
  subscription: any;
  data: any = {};

  constructor(api: ApiService,
              eventService: EventService,
              timer: TimerService,
              popup: GlobalPopupService) {
    super(eventService, timer, popup, api);

    this.settings["cityName"] = new SettingItem(SettingTypes.STRING, {
      displayName: "City name"
    }, "Montpellier");
  }

  getCityName() {
    return this.settings.cityName;
  }
  
  ngOnInit() {
    this.listen((observer: Observer<any>) => {
      this._api.weatherCity(this.settings.cityName).subscribe((data: WeatherCityResponse) => {
        observer.next(JSON.parse(data.data));
      });
    }, (result: any) => {
      if (result.cod == 200) {
        this.loaded = true;
        this.data = result;
      }
    });
  }
}
