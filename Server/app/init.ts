import { WeatherCityInstance } from './Services/Weather/city';
import { WeatherServiceInstance } from './Services/Weather/service';

import { ModuleIntraEpitechInstance } from './Services/IntraEpitech/module';
import { IntraEpitechServiceInstance } from './Services/IntraEpitech/service';
import { UserIntraEpitechInstance } from './Services/IntraEpitech/user';

import { LeBonCoinServiceInstance } from './Services/LeBonCoin/service';
import { LeBonCoinSearchInstance } from './Services/LeBonCoin/search';

import { DiversServiceInstance } from './Services/Divers/service';
import { RssWidgetInstance } from './Services/Divers/rss';
import { AtomWidgetInstance } from './Services/Divers/atom';
import { YouTubeServiceInstance } from './Services/YouTube/service';
import { YouTubeChannelInstance } from './Services/YouTube/channel';

export let init = () => {
  let Services = [
    WeatherServiceInstance,
    IntraEpitechServiceInstance,
    LeBonCoinServiceInstance,
    YouTubeServiceInstance,
    DiversServiceInstance
  ]
  let Widgets = [
    WeatherCityInstance,
    UserIntraEpitechInstance,
    ModuleIntraEpitechInstance,
    LeBonCoinSearchInstance,
    YouTubeChannelInstance,
    RssWidgetInstance,
    AtomWidgetInstance
  ]
  return { Services, Widgets };
}